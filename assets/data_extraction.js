DataExtractor = {}

DataExtractor._formQueryString = function (params) {
    return Object.keys(params).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&')
}

DataExtractor._getResult = async function (path) {
    let query = this._formQueryString({ path })
    let res = await fetch('/data?' + query)
    return res.json()
}

// DataExtractor.showResult = async function() {
//     const panel = document.getElementById('file-browsing-left-panel')
//     panel.innerHTML = 'Loading...'
//     let res = await this._getResult(current)
//     panel.innerHTML = res.transcripts
// }

const can1 = [
    {
        "transcript": "\u5582\u5440\u4f60\u5514\u8a72\u5289\u5c0f\u59d0\u6211\u59d3\u99ac\u4fc2\u908a\u5c64\u6295\u8cc7\u9280\u884c\u6253\u569f\u35ce\u5481\u800c\u5bb6\u540c\u4f60\u505a\u756a\u500b\u7d00\u9304\u97f3\u65b9\u5514\u65b9\u4fbf\u5440\u597d\u5440\u5481\u5462\u4eca\u65e5\u4fc22019\u5e749\u670826\u865f\u70ba\u4fdd\u969c\u60a8\u7684\u6b0a\u76ca\u800c\u5bb6\u5c31\u4f60\u6240\u9078\u64c7\u5605\u6295\u8cc7\u7522\u54c1788\u6211\u54cb\u958b\u59cb\u9304\u97f3\u5605\u8acb\u554f\u4f60\u5605\u5168\u540d\u4fc2\u4fc2\u5289\u5609\u5152\u4fc2\u5481\u8acb\u554f\u4f60\u5605\u8eab\u4efd\u8b49\u865f\u78bc\u4fc2604 22\u7aae\u4eba\u8acb\u554f\u4f60\u5605\u51fa\u751f\u65e5\u671f\u4fc21971\u5e747\u670811\u865fok\u5481\u8eab\u4efd\u5df2\u7d93\u88ab\u78ba\u8a8d\u35ce\u5587\u6211\u54cb\u5c31\u800c\u5bb6\u5c31\u6703\u958b\u59cb\u5462\u500bfriend\u5605\u9304\u97f3\u5605\u4fc2\u5440\u6709\u95dc\u4e00\u822c\u80a1\u7968\u639b\u9264\u53ca\u5462\u500b\u7522\u54c1\u5605\u6027\u8cea\u53ca\u5df2\u8a18\u9304\u53ca\u7247\u662f\u5728\u90f5\u4ef6\u76f8\u8acb\u554f\u4f60\u4fc2\u54aa\u5df2\u7d93\u95b1\u8b80\u660e\u767d\u53ca\u63a5\u53d7\u7522\u54c1\u5605\u6027\u8cea\u53ca\u98a8\u96aa\u9700\u5514\u9700\u8981\u518d\u4e0a\u4f60\u8b1b\u89e3",
        "confidence:": 0.9429081082344055
    },
    {
        "transcript": "\u6709\u95dc\u5462\u500b\u7522\u54c1\u5605\u98a8\u96aa\u8acb\u53c3\u7167\u92b7\u552e\u6587\u4ef6\u4e2d\u7b2c\u56db\u81f3\u4e03\u9801",
        "confidence:": 0.9324451088905334
    },
    {
        "transcript": "\u5462\u96bb\u7522\u54c1\u4fc2\u4e00\u96bb\u9ad8\u98a8\u96aa\u53ca\u6027\u8cea\u8907\u96dc\u5605\u7522\u54c1\u53ea\u4fc2\u5bb9\u8a31\u8b49\u5238\u53ca\u671f\u8ca8\u689d\u4f8b\u4e0b\u7684\u5c08\u696d\u6295\u8cc7\u8005\u4e26\u540c\u610f\u88ab\u8996\u70ba\u5c08\u696d\u6295\u8cc7\u8005\u4e4b\u5ba2\u6236\u5462\u96bb\u7522\u54c1\u4e00\u65e6\u767c\u751f\u89f8\u767c\u4e8b\u4ef6\u4e8b\u6703\u88ab\u90e8\u5206\u6216\u5168\u6578\u9435\u5c07\u5ba2\u6236\u9700\u91cd\u65b0\u7559\u610f\u5462\u96bb\u7522\u54c1\u82e5\u7121\u6cd5\u7dad\u6301\u71df\u904b\u4e8b\u4ef6\u5c07\u7531\u76e3\u7ba1\u6a5f\u69cb\u6c7a\u5b9a\u767c\u884c\u5546\u662f\u5426\u7121\u6cd5\u7dad\u6301\u71df\u904b\u5152\u8a72\u6c7a\u5b9a\u53ef\u4ee5\u662f\u767c\u884c\u5546\u7684\u63a7\u5236\u7bc4\u570d\u4e4b\u5916\u56e0\u70ba\u5c0d\u65bc\u7121\u6cd5\u7dad\u6301\u8a8d\u904b\u4e8b\u4ef6\u4e4b\u6c7a\u5b9a\u60c5\u4e5f\u4e0d\u78ba\u5b9a\u6027\u6709\u95dc\u4e8b\u4ef6\u4ee5\u53ca\u76f8\u7576\u6487\u8cec\u5982\u9700\u8981\u5c07\u5f88\u96e3\u9810\u6e2c\u4efb\u4f55\u8de1\u8c61\u6a19\u793a\u767c\u884c\u5546\u6b63\u6b65\u5411\u7121\u6cd5\u7dad\u6301\u71df\u904b\u6642\u5e7e\u6642\u53ef\u80fd\u5230\u6642\u5462\u500b\u7522\u54c1\u50f9\u683c",
        "confidence:": 0.9476625919342041
    },
    {
        "transcript": "\u5f71\u97ff\u5728\u7121\u6cd5\u7dad\u6301\u71df\u904b\u4e8b\u4ef6\u767c\u751f\u6642\u5ba2\u6236\u53ef\u80fd\u6703\u5931\u53bb\u6295\u8cc7\u65bc\u5462\u500b\u7522\u54c1\u6240\u6709\u91d1\u984d\u4ee5\u53ca\u5462\u96bb\u7522\u54c1\u7684\u8a08\u5283\u7ae0\u7a0b\u53ca\u6216\u92b7\u552e\u6587\u4ef6\u7684\u5167\u5bb9\u672a\u7d93\u4efb\u4f55\u76e3\u7ba1\u6a5f\u69cb\u5be9\u6279\u6216\u8a8d\u53ef\u4ee5\u4f60\u61c9\u5c31\u6709\u95dc\u8981\u7d04\u8b39\u614e\u884c\u4e8b\u5982\u4f60\u5c0d\u672c\u6587\u4ef6\u7684\u4efb\u4f55\u5167\u5bb9\u6709\u4efb\u4f55\u7591\u554f\u4f60\u61c9\u5c0b\u6c42\u7368\u7acb\u5c08\u696d\u610f\u898b\u8acb\u554f\u865f\u554f\u984c\u5462\u500b\u7522\u54c1\u5605\u8a73\u7d30\u689d\u6b3e\u5df2\u7d93\u4fc2term sheet\u548coffering circular\u4e2d\u5217\u660e\u56e0\u70ba\u4eca\u6b21\u4fc2\u79c1\u4eba\u914d\u552e\u5605\u95dc\u4fc2\u6240\u4ee5\u4e26\u5187\u6709\u95dc\u767c\u884c\u4eba\u6216\u7968\u64da\u8cc7\u767c\u884c\u7ae0\u7a0b\u9001\u4ea4\u516c\u53f8\u8a3b\u518a\u8655\u8a3b\u518a\u800c\u5bb6\u6703\u8b1b\u89e3\u4e0b\u5462\u96bb\u7522\u54c1\u5605\u4e3b\u8981\u5341\u9ede\u7540\u4f60\u53c3\u8003\u8a73\u60c5\u8acb\u7d30\u95b1\u6709\u95dc\u6587\u4ef6\u5462\u5572\u6587\u4ef6\u53ea\u63d0\u4f9b\u82f1\u6587\u7248\u672c\u5982\u679c\u4f60\u672a\u80fd\u5b8c\u5168\u95b1\u8b80\u53ca\u660e\u767d\u672c\u6587\u4ef6\u5167",
        "confidence:": 0.9445909261703491
    },
    {
        "transcript": "\u8acb\u4e0d\u8981\u9032\u884c\u4ea4\u6613\u53ea\u662f\u767d\u8acb\u5c0b\u6c42\u7368\u7acb\u610f\u898b\u4ee5\u4e86\u89e3\u7522\u54c1\u53ca\u672c\u6587\u4ef6\u5f8c\u624d\u9032\u884c\u4ea4\u6613\u6307\u793a\u5481\u800c\u5bb6\u5462\u5c31\u6703\u540c\u4f60\u5c0d\u8fd4\u5572\u500bfriend\u5605\u7522\u54c1\u5c31\u5f97\u35ce\u5587\u5481\u5462\u5c31\u4fc2hsbc Bank PLC HK t61 static variable fixed coupon interest rate affect stock name\u5c31\u4fc2\u5409\u5229\u6c7d\u8eca175 hk\u5605number\u4fc2hs 1909 24001\u4eba\u6cbb\u6e2f\u5e6310a 6\u500b\u6708initial fixing date\u4fc29\u670824\u865f2019\u5e7410\u670810\u865f2019\u5e74final\u8056\u5730\u4fc24\u670814\u865f2020\u5e744\u670821\u865f2020\u5e74price",
        "confidence:": 0.9488214254379272
    },
    {
        "transcript": "97.5 coupon\u4fc212.83 km Daily",
        "confidence:": 0.8700728416442871
    },
    {
        "transcript": " call\u6d3e718",
        "confidence:": 0.7614957690238953
    },
    {
        "transcript": " login script API login password 65\u6211\u54cb\u4eca\u6b21\u4fc2\u6703\u8cb7\u6e2f\u5e63\u5605\u4e8c\u767e\u842c\u5605\u5481\u5462\u500bproduct risk rating label\u4fc2\u4e94\u5605",
        "confidence:": 0.930923581123352
    },
    {
        "transcript": " \u5481\u8acb\u554f\u4e0a\u5605\u8cc7\u6599\u4f60\u6709\u5187\u554f\u984c\u4f60\u4fc2\u54aa\u5df2\u7d93\u6536\u5230\u540c\u57cb\u95b1\u8b80\u6709\u95dc\u92b7\u552e\u6587\u4ef6\u5305\u62ectimesheet\u548cofficer\u7279\u5225\u4fc2\u6709\u95dc\u98a8\u96aa\u5c07\u8a2d\u5b9a\u5b8c\u5168\u7406\u89e3\u53ca\u540c\u610f\u7576\u4e2d\u7684\u5167\u5bb9\u53ca\u98a8\u96aa\u4e26\u627f\u53d7\u4e26\u63a5\u53d7\u76f8\u95dc\u7684\u689d\u6b3e\u5ba2\u6236\u5fc5\u9808\u77e5\u8b58\u6b64\u7522\u54c1\u5bb9\u8a31\u6295\u8cc7\u8005",
        "confidence:": 0.9456819295883179
    },
    {
        "transcript": "\u5df2\u767c\u751f\u751f\u6548\u4e8b\u4ef6\u5982\u9069\u7528\u53ca\u6709\u95dc\u53c3\u8003\u8cc7\u7522\u65bc\u5c46\u6eff\u65e5\u4e4b\u6536\u5e02\u50f9\u540c\u6642\u4f4e\u65bc\u884c\u4f7f\u50f9\u5341\u8d16\u56de\u5bb6\u6642\u5982\u9069\u7528\u53ef\u4ee5\u9078\u64c7\u73fe\u91d1\u6216\u5931\u7269\u7d50\u7b97\u4f46\u662f\u900f\u904e\u672c\u884c\u8a8d\u8cfc\u7684175\u80a1\u7968\u639b\u9264\u6295\u8cc7\u5c07\u8fd1\u4e8c\u5341\u7269\u7d50\u7b97\u532f\u8c50\u4ea6\u4e0d\u6703\u63d0\u4f9b\u4efb\u4f55\u6cd5\u5f8b\u76e3\u7ba1\u53ca\u7a05\u52d9\u8ca1\u52d9\u4e0a\u9047\u898b\u4f60\u4fc2\u54aa\u660e\u767d\u53ca\u63a5\u53d7\u5462\u500b\u7522\u54c1\u5605\u767c\u884c\u4e0a\u569f\u5605",
        "confidence:": 0.935661256313324
    },
    {
        "transcript": "\u672c\u884c\u5df2\u9700\u8acb\u554f\u4f60\u4eca\u6b21\u4ea4\u5605\u6295\u8cc7\u76ee\u7684\u4fc2\u4e00\u4fdd\u672c\u4ee5\u524d\u5728\u73fe\u91d1\u6536\u76ca\u6216\u4e09\u6f5b\u5728\u8cc7\u672c\u589e\u503c\u53ca\u6216\u6f5b\u5728\u975e\u73fe\u91d1\u6536\u76ca\u73fe\u91d1\u6536\u76ca\u672c\u53ef\u4ee5\u5c31\u5ba2\u6236",
        "confidence:": 0.9260947704315186
    },
    {
        "transcript": "\u6295\u8cc7\u8a55\u4f30\u554f\u5377\u4e2d\u5605\u6295\u8cc7\u76ee\u6a19\u53ca\u610f\u5411\u98a8\u96aa\u8a55\u4f30\u985e\u5225\u548c\u6295\u8cc7\u5e74\u671f\u4ee5\u53ca\u4ea4\u6613\u7684\u6295\u8cc7\u76ee\u7684\u548c\u7522\u54c1\u8cc7\u7522\u96c6\u4e2d\u98a8\u96aa\u7540\u500b\u9805\u76ee\u5c0d\u5ba2\u6236\u8cfc\u8cb7\u5605\u6295\u8cc7\u7522\u54c1\u51fa\u7522\u54c1\u9069\u5408\u6027\u8a55\u4f30\u6709\u95dc\u860b\u679c\u7d50\u679c\u4fc2\u5b8c\u5168\u4e0d\u6703\u5605\u6709\u95dc\u5462\u500b\u8a55\u4f30\u7d50\u679c\u5605\u7d30\u7bc0\u9700\u5514\u9700\u8981\u518d\u5411\u4f60\u89e3\u91cb\u6709\u95dc\u860b\u679c\u7d50\u679c\u6703\u7a0d\u5f8c\u90f5\u5bc4\u7d66\u4f60\u82e5\u6709\u7591\u554f\u8acb\u806f\u7d61\u6211",
        "confidence:": 0.9399815797805786
    },
    {
        "transcript": "\u4f60\u4fc2\u54aa\u5df2\u7d93\u5b8c\u5168\u660e\u767d\u92b7\u552e\u6587\u4ef6\u4e2d\u5305\u62ec\u6700\u65b0\u5605dum\u5566\u5c31\u6700\u65b0\u5605product booklet\u5566\u4fc2\u532f\u8c50\u5206\u884c\u5605\u540c\u57cb\u6700\u65b0\u5605financial disclosure document\u90fd\u4fc2\u6709\u532f\u8c50\u5206\u884c\u53ca\u4e86\u89e3\u6240\u9078\u5605\u6295\u8cc7\u7522\u54c1\u5305\u62ec\u7522\u54c1\u5167\u5bb9\u6295\u8cc7\u5e74\u671f\u6295\u8cc7\u91d1\u984d\u98a8\u96aa\u53ca\u6f5b\u5728\u56de\u5831",
        "confidence:": 0.9434483647346497
    },
    {
        "transcript": "\u5481\u672c\u884c\u66fe\u7d93\u5411\u4f60\u63d0\u4f9b\u8003\u616e\u5605\u5176\u4ed6\u6295\u8cc7\u7522\u54c1\u5c31\u67095\u865f\u6ed9\u8c50\u5605\u4e00\u500b\u639b\u9264\u5605\u884c\u4e8b\u4ee5\u4e3b\u4e8b\u4eba\u8eab\u4efd\u884c\u4e8b\u5605\u9280\u884c\u4e26\u975e\u7368\u7acb\u7684\u4e2d\u4ecb\u4eba\u5b9a\u7fa9\u5728\u90f5\u4ef6\u4e0a\u62ab\u9732\u9280\u884c\u53ef\u80fd\u6536\u53d6\u5c24\u5176\u4ed6\u4eba\u58eb\u53ef\u80fd\u7269\u5305\u62ec\u7522\u54c1\u653e\u5b78\u4eba\u91d1\u9322\u53ca\u6216\u975e\u91d1\u9322\u6536\u76ca\u9280\u884c\u5c31\u6b64\u7533\u8acb\u53ef\u80fd\u63a5\u53d7\u5f9e\u7522\u54c1\u767c\u884c\u4eba\u6240\u63d0\u4f9b\u7684\u91d1\u9322\u6240\u4ee5\u70ba\u4e00\u500b\u63d0\u9192\u6211\u800c\u5bb6\u7528\u4f60\u4ee5\u4e0b\u5605\u6236\u53e3\u8cc7\u7522\u4ea4\u6613\u80a1\u7968\u639b\u9264\u6295\u8cc7\u6236\u53e3\u4fc2account number 1 2 3 4 5 6 7 8\u73fe\u91d1\u7d50\u7b97\u6236\u53e3\u4fc28800 0234\u6295\u8cc7\u91d1\u984d\u4fc2hkd\u6e2f\u5e63200\u842c\u7acb\u7684\u7533\u8acb\u5c07\u4e0d\u53ef\u64a4\u92b7\u6216\u64a4\u56de\u5982\u679c\u4f60\u63a5\u53d7\u6b64\u9805\u4ea4\u6613",
        "confidence:": 0.9472517967224121
    },
    {
        "transcript": "\u5373\u6642\u597d\u5206\u9280\u884c\u6709\u6b0a\u6069\u6fa4\u4e4b\u60c5\u6cc1\u800c\u53d6\u6d88\u767c\u884c\u5462\u500b\u7522\u54c1\u6211\u54cb\u6703\u76e1\u5feb\u901a\u77e5\u4f60\u767c\u884c\u7d50\u679c\u5b8c\u6210\u7acb\u591a\u8b1d\u4f60",
        "confidence:": 0.9235455393791199
    }
]

const can2 = [
    {
        "transcript": "\u70ba\u6211\u4fc2\u5440\u4eca\u65e5\u60f3\u540c\u4f60\u505a\u500bfriend\u982d\u5148\u5605\u9304\u97f3\u5605\u771f\u4fc2\u5481\u65b9\u4fbf\u4fc22019\u5e74\u56059\u670816\u6703\u5c31\u4f60\u9078\u64c7\u5605\u6295\u8cc7\u7522\u54c1hsbc HK districts\u53bb\u9032\u884c\u9304\u97f3\u5605\u8eab\u4efd\u5605\u8acb\u554f\u4f60\u963f\u5abd\u7747\u771f1970\u5e741\u67081\u865f\u597d\u5514\u8a72\u66ec\u5481\u6709\u95dc\u4e00\u822c\u80a1\u7968\u639b\u9264\u540c\u57cb\u5462\u500b\u624b\u6a5f\u5605\u6027\u8cea\u540c\u57cb\u76f8\u7247\u7d00\u9304\u986f\u793a\u4fc2\u7531\u5e7e\u6642\u569f\u35ce\u8acb\u554f\u4fc2\u54aa\u95b1\u8b80\u96f6\u516b\u6298\u6263\u5462\u500b\u7522\u54c1\u5605\u6027\u8cea\u98b1\u98a8\u96aa\u5148\u81f3\u540c\u4f60\u660e\u767d\u53c3\u8003\u92b7\u552e\u6587\u4ef6\u4e2d\u5605\u7b2c\u56db\u81f3\u4e03\u9801\u5605",
        "confidence:": 0.9468417167663574
    },
    {
        "transcript": "\u7522\u54c1\u5605\u689d\u6b3e\u5df2\u7d93\u4fc2tommy\u540c\u57cb\u6211\u60f3\u807d\u9ece\u660e\u5605\u56e0\u70ba\u4eca\u6b21\u4fc2\u79c1\u4eba\u914d\u552e\u95dc\u4fc2\u6240\u4ee5\u4e26\u7121\u4eba\u5605\u6a19\u53e5\u4e4b\u5be6\u884c\u7ae0\u7a0b\u9001\u4ea4\u516c\u53f8\u8a3b\u518a\u8655\u8a3b\u518a\u800c\u5bb6\u6703\u8b1b\u4e0b\u5462\u96bb\u7522\u54c1\u5605\u4e3b\u8981\u7279\u9ede\u7540\u4f60\u53c3\u8003\u8a73\u60c5\u8acb\u7d30\u95b1\u6709\u95dc\u6587\u4ef6\u82f1\u6587\u4ef6\u53ea\u63d0\u4f9b\u82f1\u6587\u7248\u672c\u5982\u679c\u4f60\u672a\u5b8c\u5168\u660e\u767d\u660e\u767d\u6587\u4ef6\u5167\u5bb9\u9032\u884c\u4ea4\u6613\u5e7e\u6642\u8acb\u5c0b\u6c42\u7368\u7acb\u610f\u898b\u4ee5\u4e86\u89e3\u7522\u54c1\u53ca\u672c\u6587\u4ef6\u5f8c\u624d\u9032\u884c\u4ea4\u6613\u6307\u793a\u5481\u800c\u5bb6\u6703\u8b1b\u756a\u500b\u7522\u54c1\u52e2\u8cea\u5605\u500b\u7522\u54c1\u55f0\u5572\u8aaa\u8a71\u4fc2check This is an order HSBC HK districts of debt corporate tax corporate tax rate may\u76ae\u5409\u5229\u6c7d\u8ecaseries number\u4fc2h S at 909 24001 Tomica 4d\u6211\u807d\u65e5\u4fc2\u516d\u500b\u6708\u6d88\u606f\u5347\u6a5f\u4fc22019\u5e74\u5605\u72d7",
        "confidence:": 0.94831383228302
    },
    {
        "transcript": "\u6700\u597d\u7b11\u5730\u9f4a2019\u5e74\u560510\u670810\u865f\u8fd4\u5c4b\u4f01\u5148\u5b9a\u4fc22020\u5e74\u56054\u670814\u865fmadrid HK 2020\u5e74\u56054\u670821\u865fin surprise\u4fc2197.5\u5206\u7684\u6d88\u9632\u4fc2\u5341\u4e8c\u9ede\u5341\u4e8c\u9ede\u516b\u4e09t a coupon\u4fc2\u540cpenny price NAS daily login password\u5605\u5481\u8ddf\u6211\u505a\u904b\u52d5\u5440\u4fc2\u6e2f\u7d19\u5605200\u842c\u5566\u597d\u5572\u6c34\u77e5\u8b58\u7522\u54c1\u5165\u53bb\u6295\u8cc7\u8005\u65bc200\u751f\u751f\u8003\u8a66\u9ede\u5982\u9069\u7528\u53ca\u6709\u95dc\u53c3\u8003\u8cc7\u7522\u4e00\u5c46\u6eff\u65e5\u8a8c\u6536\u6642\u9593\u540c\u4e8b\u7b49\u65bc\u884c\u4f7f\u50f9\u6536\u56de\u50f9\u9322\u9069\u7528\u65bc\u73fe\u91d1\u53ca\u98df\u7269\u7684\u9078\u55ae\u900f\u904e\u672c\u884c\u8a8d\u8cfc\u5605175\u80a1\u7968\u639b\u9264\u6295\u8cc7\u5c07\u8fd1\u4e8c\u5341\u516d\u8a08\u7b97\u4e00\u96bbcd\u4fc2\u5514\u6703\u63d0\u4f9b\u4efb\u4f55\u6cd5",
        "confidence:": 0.9448646903038025
    },
    {
        "transcript": "\u5187\u4e0a\u5e8a\u597d\u5beb\u610f\u601d\u4f60\u4fc2\u54aa\u660e\u767d\u540c\u63a5\u53d7",
        "confidence:": 0.8585416078567505
    },
    {
        "transcript": "\u672c\u6eff\u610f\u5c31\u5ba2\u6236\u55ba\u982d\u5148\u8a55\u4f30\u554f\u5377\u4e2d\u5605\u6295\u8cc7\u76ee\u6a19\u540c\u57cb\u4ee5\u5f8c\u98a8\u96aa\u8a55\u4f30\u985e\u5225\u540c\u6295\u8cc7\u5e74\u671f\u4ea4\u6613\u5605\u6295\u8cc7\u76ee\u7684\u540c\u7522\u54c1\u96c6\u8cc7\u7522\u96c6\u4e2d\u98a8\u96aa\u6bd4\u7387\u5462\u4e94\u500b\u9805\u76ee\u5c0d\u5ba2\u6236\u5462\u6b21\u8cfc\u8cb7\u5605\u6295\u8cc7\u7522\u54c1\u4f5c\u51fa\u9069\u5408\u6027\u8a55\u4f30\u8a55\u4f30\u7d50\u679c\u4fc2\u5b8c\u5168\u4e0d\u6703\u5605\u6709\u95dc\u5462\u500b\u860b\u679c\u5605\u7d30\u540c\u4f60\u518d\u8b1b\u89e3\u5514\u9700\u8981\u53ef\u4ee5\u518d\u806f\u7d61",
        "confidence:": 0.9450863003730774
    },
    {
        "transcript": "\u8acb\u4f60\u4fc2\u54aa\u5df2\u7d93\u5b8c\u5168\u5514\u660e\u767d\u7c3d\u6536\u6587\u4ef6\u4e2d\u5305\u62ecarray to String to array to store data in financial disclosure document format cc",
        "confidence:": 0.8677130341529846
    },
    {
        "transcript": "\u540c\u57cb\u4e86\u89e3\u5462\u500b\u6295\u8cc7\u7522\u54c1\u5305\u62ec\u7522\u54c1\u5916\u50ad\u6295\u8cc7\u524d\u671f\u6295\u8cc7\u91d1\u984d\u98a8\u96aa\u53ef\u6301\u7e8c\u56de\u5831\u660e\u767d\u6211\u66fe\u7d93\u540c\u8003\u616e\u7684\u63d0\u4f9b\u8003\u616e\u6295\u8cc7\u7522\u54c1\u4fc2\u6709\u7b2c\u4e8c\u6a23\u975a\u5605\u51faseason 3\u5fb7\u83ef\u70ba\u80a1\u7968\u4fc2780\u5605\u4fc2\u672c\u90fd\u4fc2\u4ee5\u4f4f\u662f\u4eba\u8eab\u4efd\u884c\u4e8b\u5605\u4eba\u6211\u54cb\u975e\u7368\u7acb\u5605\u4e2d\u4ecb\u4eba\u5df2\u7d93\u4fc2\u7531\u96fb\u68af\u5230\u9280\u884c\u53ef\u80fd\u6536\u53d6\u5176\u4ed6\u4eba\u58eb\u5305\u62ec\u7522\u54c1\u767c\u884c\u4eba\u91d1\u9322\u53ca\u8cc7\u91d1\u5b58\u6536\u76ca",
        "confidence:": 0.9472296833992004
    },
    {
        "transcript": "\u8fd1\u770b\u5c31\u662f\u7533\u8acb\u53ef\u80fd\u63a5\u53d7\u91cd\u65b0\u5317\u623f\u5ba2\u4eba\u76f4\u63a5\u6216\u9593\u63a5\u6240\u63d0\u4f9b\u5605\u91d1\u9322\u6536\u4e00\u500bpercent\u5605\u5481\u6211\u54cb\u800c\u5bb6\u6703\u7528\u4f60\u5605\u4ee5\u5f8c\u597d\u4f3c\u4fc2\u4ea4\u6613\u5566\u80a1\u7968\u6236\u53e3\u4fc21678\u73fe\u91d1\u7d50\u7b97\u6236\u53e3\u4fc28 8 0 8 0 0 0 1 2 3 4 5\u6295\u8cc7\u91d1\u984d\u4fc2\u6e2f\u5e63\u5605\u4e8c\u767e\u842c\u5605\u6307\u7532\u8ab2\u7a0b\u7533\u8acb\u4fc2\u5514\u597d\u610f\u601d\u8a71\u6642\u6703\u5566\u5982\u679c\u5462\u500b\u6a59\u5462\u9805\u4ea4\u6613",
        "confidence:": 0.9363546967506409
    },
    {
        "transcript": "\u5373\u6642\u597d\u8fd4\u5605\u9280\u884c\u6709\u6b0a\u56e0\u6b64\u60c5\u6cc1\u800c\u53d6\u6d88\u5316\u4e0b\u5462\u500b\u7522\u54c1\u6211\u54cb\u6703\u76e1\u5feb\u901a\u77e5\u4f60\u5462\u500b\u76f8\u95dc\u7d50\u679c\u5605",
        "confidence:": 0.9249032735824585
    },
    {
        "transcript": "\u68af\u5f62\u6210\u8a72\u66ec\u591a\u8b1d\u66ec\u62dc\u62dc",
        "confidence:": 0.9329246282577515
    }
]

const eng1 = [
    {
        "transcript": "I got to my schedule is now. Okay, so, my name is Desmond. I'm calling from CCC iPod bank. So I'm going to throw less calling with you regarding the English and photo ID to speak now. Okay? Okay. Okay. Today is the Chinese September of 2009 to protect your interest. We will stop auto wrecking for your Cilla T. Ohh n policies issued by HSBC to TD Auto mobile whole Disneyland.",
        "confidence:": 0.6072477102279663
    },
    {
        "transcript": "1752 to ensure that you are the account holder, please provide your full name, please. Hello. How are you? Amy and your ID card number is a one two three, four, five six seven eight. And your birth date is 2",
        "confidence:": 0.6117812395095825
    },
    {
        "transcript": "My jeans on your wall November 1st.",
        "confidence:": 0.7019460201263428
    },
    {
        "transcript": " Okay, so your identity is confirm that.",
        "confidence:": 0.6404011845588684
    },
    {
        "transcript": " The nature and the pieces of the um photos as well as the debt that you have been well and stay on the bill if you read Alice to an asset the laser and mrs. Of the photos and need to explain to you again. Oh, no, no, please refer to page 427 of the sales documents for the wishes of the product.",
        "confidence:": 0.6250898241996765
    },
    {
        "transcript": " Billy's does wanted spent the statement 46 SPC year and it is a high with Encompass Pollard apologies Target at Pappadeaux investor SD V insecurities and fill disorder. Only portal is subject to the release of being beaten path customer read tester office of long liability event is dependent on the data, Mass.",
        "confidence:": 0.5739793181419373
    },
    {
        "transcript": "Fly directly touch off the loan Wapiti of the Easter and may be outside resource control because of the inhaler and 70 we kinda determination of whether a Lauren labret even exist. It will be difficult to predict when if at all right off we offer any indication tester easily turned into us along with any event could have an adverse effect on the market price of the phone system. I may lose office investment in advance in the event that our lawn Barbara van Oakhurst this private placement academic note last night up in we will not endorse by any record property in Hong Kong before making any investment assistant. Please carefully read all the related offer document or Port Townsend sick for independent and professional Financial test at La simply.",
        "confidence:": 0.6139514446258545
    },
    {
        "transcript": " Terms have been nice.",
        "confidence:": 0.7229933738708496
    },
    {
        "transcript": "On the term sheet and Office document as this is a placement as low post.",
        "confidence:": 0.6583216190338135
    },
    {
        "transcript": " Test test note posted about easel another note required to be sent to the company's policy for registration purposes the office or come AV for English only there for customer Steve policy in English 1/2 of independent provider at West and fully understanding familiarize forward in the open documents before this pace of alarming policy, just 11th. Please refer to the realtor the house, you know, and I will now explain to you",
        "confidence:": 0.5771477222442627
    },
    {
        "transcript": " So the easier of this policy fast busy Banks PLC Department name is HSBC x86 monthly recap of the scoop on your end with a meeting up at the store name is install cookies TD Auto mobile. Holy family. This is 175 Hong Kong. See this number is s 19092.",
        "confidence:": 0.6090602278709412
    },
    {
        "transcript": "Open Clancy's Haagen-Dazs for the 1086 months knees office in Vegas Valley 4th of September 2:01. Nine three stages 10th of October to 190 final fishing Pages 14th of April 2020. Their maturity date is 21st of April 2020 surprises 100% flight prices 97.5% of execute funniest 12.3 present for Anna and the cop of features is Bailey.",
        "confidence:": 0.6683609485626221
    },
    {
        "transcript": "Paul Ponder person login feature cki in price is 65% and investment money investing you to meet in Hong Kong dollars.",
        "confidence:": 0.652300238609314
    },
    {
        "transcript": " So have you be safe and all the open documents including times and often Sevilla in packet in Portland this morning and this?",
        "confidence:": 0.620564341545105
    },
    {
        "transcript": "One thing sessions and fully understood 11th Lisa single. Yes customer at Loma Vista Place in Massachusetts at the cash settlement or physical server in the case where the login U.S.A. If a couple and coolant hose and pass out the reference SS on every page bit low post office supplies, and the court is available. However for HSBC Easter here and Squad Food Bank only physical Stedman reply excess message is not for energy Co record of test account until Econo at wife.",
        "confidence:": 0.5885801315307617
    },
    {
        "transcript": "Let us know, okay.",
        "confidence:": 0.5763033032417297
    },
    {
        "transcript": " So they're going to assess your survey for investing in disposed at the Englishmen also type of leasing office. Paula placement is to achieve reschedule, please say agent or poledo cash income or Paula.",
        "confidence:": 0.6265047788619995
    },
    {
        "transcript": "Settle opposition and or potential on cash income potential cash income.",
        "confidence:": 0.6175492405891418
    },
    {
        "transcript": " Okay.",
        "confidence:": 0.8375823497772217
    },
    {
        "transcript": " I'll calling to your investment profile questionnaire, which county softer customers Palmer English Mastiff an attitude the res profile investment on Hawthorne and English marchetti faulty Sanderson an essay College loan proposal Obispo Tempe Saint Pete 5 items to assess your therapy for English 10th Espada the resale of the assessment a company match that you need us to explain to you the results of assessment or not. Okay. Thank you.",
        "confidence:": 0.595158576965332
    },
    {
        "transcript": " so",
        "confidence:": 0.7799122929573059
    },
    {
        "transcript": " have you fully understand a sales documents top test the latest program event on the latest for a bullet and the latest Financial disclosure document from the HSBC the time sheet and for Ava Mason Terrace, Miami and",
        "confidence:": 0.6503967046737671
    },
    {
        "transcript": "Hansel with us and return of such investment. Yes.",
        "confidence:": 0.603922426700592
    },
    {
        "transcript": " Please note that the alternative investment for the debt. We have been pause for your account has recently found 7x X Hong Kong your end.",
        "confidence:": 0.6199305653572083
    },
    {
        "transcript": " So",
        "confidence:": 0.8375823497772217
    },
    {
        "transcript": " the Bania Center in new the transition calling Phyllis ESPN support. The ban is not independent in -8 is also been disclosed in the mail that the Fenway receive quality and all normally benefits from Oliver's which may include process the bank. May I set the monetary purpose for white bottle pleased with our website or indirectly from the application. So not a sitter investment of the transition of 1%",
        "confidence:": 0.588723361492157
    },
    {
        "transcript": " Kia of your investment account information for this transition the fuck sure that you are condom parties.",
        "confidence:": 0.6080182194709778
    },
    {
        "transcript": "2347 five six seven and the settlement on embodies 987-654-3213 Westmont Mondays home address to 2 million or applications acceptable or if you're if you if you accept this Cancer Center Bellevue immediately who found the been made out last logged into the total service on Monday. We advise New Daughter Marissa as soon as possible to you know, say yes.",
        "confidence:": 0.6007187962532043
    },
    {
        "transcript": "Best oil for the transition completed. Thank you for your time. Thank you. Bye. Bye. See you. Bye. Bye.",
        "confidence:": 0.6821929812431335
    }
]

const eng2 = [
    {
        "transcript": "\u70bap30 s10e smart Tool for the weekend for your heart is OK OK So Today is a quick shortcut to protect your interest will Start on the waiting for your submit the product of sum in Hong Kong Stock issued by HSBC Bank to ensure that your account please provide your full name of the status Hong Kong a1234567\u5fae\u4fe1you are confirmed the nature and service",
        "confidence:": 0.9175145030021667
    },
    {
        "transcript": "product as of the selected And have been recognized on the people Have you will understand and accept the nature and results of the production to explain to you again please set up to present of the shares of the interests of the product",
        "confidence:": 0.9242563843727112
    },
    {
        "transcript": " Japan has been received on the terms and order to meet at this is a private placement is no problem about in order not required to present the registration process may be Applied in English only That for customers should be in English or have thought in a professional Advisor and fully understand the information provided in the offering documents provided for the taste are the main points of presence of this HSBC",
        "confidence:": 0.9343103170394897
    },
    {
        "transcript": "Mannings HSBC hkex monthly record with other than the man with the autumn SUN Mobile Holdings Limited stock Hong Kong companies act as in icao line Hong Kong AT Initial fixing date is of no one is the stance of final fixing date is able to take a photo to external hard drive for this is connected person per annum interest at the center login status not in prices and the investment amount for this connection is terminated Hong Kong dollars",
        "confidence:": 0.941788375377655
    },
    {
        "transcript": "and read the offering documents including the time and offering circular in work in other important with warnings and visualization and fully understood as double quote the price of the website access on next Friday is but no later than the cost is effective for HSBC subscribe for the HSBC Swift Code only apply HSBC supplementary go to the order of accounting ok\u6211\u89ba\u5f97investment profile questionnaire with condition of the customers",
        "confidence:": 0.9276179075241089
    },
    {
        "transcript": "active active service portfolio investment Horizon and investment committee for the sun and the proportion of these products based on these items to your Beautiful in this product the results of this assessment a comparative study notes to explain With You used to explain the result of assessment Thank you have you come to the latest programming random array to store the blood and related financial disclosure document from the HSBC terms and the policy Information technology investment amount and Return of investment",
        "confidence:": 0.9430007934570312
    },
    {
        "transcript": "ok\u6211\u60f3investment products have been to provide for",
        "confidence:": 0.9021769165992737
    },
    {
        "transcript": "13X Hong Kong Stock with also This is the end",
        "confidence:": 0.8902162909507751
    },
    {
        "transcript": " Show the bank is internal transaction contemplated herein as principal the band is not independent intermediary It has also Been disclosed to receive money tree and normal delivery system idle process Which is the best way to set the moral of the story of the product is not directly or indirectly stations are not exceeding amount of the transaction of one doesn't have our accounting information for decisions of your account number from one two three Prime numbers and is in order",
        "confidence:": 0.9269899129867554
    },
    {
        "transcript": "tumi Hong Kong bus Stop it yourself is in the blank field image decode json double may not connect to the remote session Is Possible to know if it is OK for the percentage complete thank you",
        "confidence:": 0.9259678721427917
    }
]

const man1 = [
    {
        "transcript": "\u70ba\u4f60\u597d\u4f3c\u7121\u7dda\u4e0a\u7db2\u90fd\u4ef2\u6709\u5c11\u5c11\u6211\u6703\u4fdd\u91cd\u5440\u561b\u54aa\u80e1\u8001\u5e2b\u5c0d\u4f4f\u5bbf\u9910\u5ef3\u5605\u98a8\u6247",
        "confidence:": 0.9015883207321167
    },
    {
        "transcript": "\u97f6\u95dc\u51fa\u53bb\u9806\u4fbf\u5605\u98a8\u6247\u6b98\u5ea7\u6578",
        "confidence:": 0.8587321043014526
    },
    {
        "transcript": "\u5e73\u6642\u4f62\u6703\u8a08\u7b97\u76f8\u5c0d\u516c\u53f8\u5546\u6703",
        "confidence:": 0.844966471195221
    },
    {
        "transcript": "\u5277\u5e73\u5605\u6240\u6709\u88dd\u4e0a\u6b21\u56e0\u70ba\u6240\u4ee5\u70ba\u6b7b\u5497\u9ede\u89e3\u4f60\u8981\u6211\u5168\u90e8\u6709\u9322\u6536",
        "confidence:": 0.9430328607559204
    },
    {
        "transcript": "HSBC Bank PLC HSBC HK deposit\u8aaa\u5730\u8aaa\u6211\u8981\u505a\u5622\u90fd\u662f\u6709\u5c0f\u5df478k",
        "confidence:": 0.9507024884223938
    },
    {
        "transcript": "offering circular pvc\u6709\u95dc\u98a8\u6247\u88dd\u7f6e\u500b\u4eba\u9700\u8981\u8acb\u4f60\u5462\u6de8\u4fc2\u4e00\u500b\u5b78\u751f",
        "confidence:": 0.8562783598899841
    },
    {
        "transcript": "\u8acb\u5728\u4f38\u5c55\u6536\u4e8c\u6211\u6536\u4fe1\u6587\u7ae0\u4e2d\u5305\u62ec\u7368\u7acbsong is the latest financial disclosure document for licensed CCC",
        "confidence:": 0.8891738653182983
    },
    {
        "transcript": "\u9f8d\u5410\u73e0\u4eba\u4f3c\u5514\u4f3c\u7238\u7238\u76f8\u7247\u6642\u82b1\u6ffa\u91cf\u6e05\u7246\u7d19\u904e\u975e\u6027\u9a37\u64fe\u5065\u5eb7\u5c31\u662f\u751f\u5b58\u56f0\u96e3\u5f1f\u795ege google\u662f\u7238\u7238\u7238\u4e86",
        "confidence:": 0.8885565996170044
    },
    {
        "transcript": "\u7a7f\u5f71\u816e\u817a\u585e\u8eca\u904e",
        "confidence:": 0.7307742238044739
    }
]

const man2 = [
    {
        "transcript": "\u70ba\u4ec0\u9ebc\u984f\u8272\u846b\u8606\u523a\u773c\u76ae\u5605\u6642\u5019\u98a8\u6247\u7c21\u5beb",
        "confidence:": 0.8495126962661743
    },
    {
        "transcript": "\u6240\u4ee5\u4e0d\u6703\u6709\u95dc\u8fd4\u96ea\u7a2e\u9322\u524d\u59bb\u524d\u66f8\u8981\u95dc\u73a9\u9806\u4fbf\u8a66\u8a66bank PLC + HSBC HKD to TWD",
        "confidence:": 0.8678504824638367
    },
    {
        "transcript": "HSBC hkd\u5df470a minibus hk\u8acb\u554f\u9ede\u8212\u670d\u5df2\u7d93\u6536\u5230\u6211\u8cbb\u4e8b\u540c\u610f\u7576\u4e2d\u7684\u982d\u9aee",
        "confidence:": 0.9210203886032104
    },
    {
        "transcript": "\u8001\u9f20\u809a\u5b50\u6709\u95dc\u89aa\u5462\u5c07\u8ecd\u6fb3\u8212\u670d\u53e6\u5916\u8acb\u53d6\u6d88\u8f49\u6578\u6613\u5b78\u751f\u8acb\u5047\u6c92\u6709\u9322\u4eba\u98a8\u60c5\u666f\u89c0",
        "confidence:": 0.9061048626899719
    },
    {
        "transcript": "\u5730\u7403\u597d\u5371\u96aa\u7b2c\u4e09\u7d20\u69ae\u748b\u9673\u76ae\u9673\u76ae\u982d\u5148\u4e0a\u5e73\u53f0",
        "confidence:": 0.8090252876281738
    },
    {
        "transcript": "\u5f71\u97ff\u5176\u4ed6\u9078\u5169\u96bb\u5c0f\u57f9\u6b63\u5c0f\u6578\u7d19\u516c\u4ed4\u653f\u60c5\u8607\u8449",
        "confidence:": 0.7709242701530457
    }
]

DataExtractor.showResult = function (path) {
    const panel = document.getElementById('file-browsing-left-panel')

    while (panel.firstChild) {
        panel.removeChild(panel.firstChild);
    }

    iterators = []
    switch (path) {
        case 'Cantonese2.wav':
            iterators = can2
            break
        case 'Cantonese1.wav':
            iterators = can1
            break
        case 'English1.wav':
            iterators = eng1
            break
        case 'English2.wav':
            iterators = eng2
            break
        case 'Mandarin1.wav':
            iterators = man1
            break
        case 'Mandarin2.wav':
            iterators = man2
            break
        default:
            break
    }

    console.log(iterators)

    for (let i = 0; i < iterators.length; i++) {
        const div = document.createElement('div')
        div.innerHTML = iterators[i].transcript
        console.log(i, iterators[i], iterators[i].transcript, iterators[i]['confidence:'])
        if (iterators[i]['confidence:'] > 0.9) {
            div.className = 'confident'
        }

        panel.appendChild(div)
    }
}